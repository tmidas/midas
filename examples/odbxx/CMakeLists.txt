cmake_minimum_required(VERSION 3.14)
project(odbxx_test)
if(NOT TARGET midas::midas)
    find_package(Midas REQUIRED PATHS $ENV{MIDASSYS})
endif()

add_executable(odbxx_test odbxx_test.cxx)
target_link_libraries(odbxx_test midas::midas)
