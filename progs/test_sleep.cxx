//
// test_sleep.cxx
//
// Test the sleep function
//

#include <stdio.h>
#include <signal.h> // SIGPIPE
#include <unistd.h> // alarm()
//#include <assert.h> // assert()
//#include <stdlib.h> // malloc()
//#include <math.h> // M_PI

//#include "midas.h"
#include "tmfe.h"

void test(double total_sleep, double call_sleep)
{
   double start_time = TMFE::GetTime();
   double count = 0;
   double incr = 0;

   int loops = total_sleep/call_sleep;

   for (int i=0; i<loops; i++) {
      TMFE::Sleep(call_sleep);
      count += 1.0;
      incr += call_sleep;
   }
   
   double end_time = TMFE::GetTime();

   double elapsed = end_time - start_time;
   double actual_sleep = elapsed/count;

   printf("sleep %7.0f loops, %12.3f usec per loop, %12.9f sec total, %12.9f sec actual total, %12.3f usec actual per loop, oversleep %8.3f usec, %6.1f%%\n",
          count,
          call_sleep*1e6,
          incr,
          elapsed,
          actual_sleep*1e6,
          (actual_sleep - call_sleep)*1e6,
          (actual_sleep - call_sleep)/call_sleep*100.0);
}

void test_alarm(int)
{
   printf("alarm!\n");
}

int main(int argc, char* argv[])
{
   setbuf(stdout, NULL);
   setbuf(stderr, NULL);

   signal(SIGPIPE, SIG_IGN);
   signal(SIGALRM, test_alarm);

   //TMFE::Sleep(-1.0);
   //TMFE::Sleep(-0.0);
   //TMFE::Sleep(+0.0);
   //TMFE::Sleep(0);
   //TMFE::Sleep(12345678901234567890.0);

   //::alarm(2);
   
   //TMFE* mfe = TMFE::Instance();

   if (1) {
      printf("test short sleep:\n");

      test(1.0, 0.1);
      test(1.0, 0.01);
      test(1.0, 0.001);
      test(1.0, 0.0001);
      test(1.0, 0.00001);
      test(1.0, 0.000001);
      test(0.1, 0.0000001);
      test(0.01, 0.00000001);
   }

   if (1) {
      double sleep_requested = 2.1*60; // 2.1 minutes
      printf("test long sleep: requested %.9f sec ... sleeping ...\n", sleep_requested);
      ::alarm(10);
      double t0 = TMFE::GetTime();
      TMFE::Sleep(sleep_requested);
      double t1 = TMFE::GetTime();
      double sleep_actual    = t1 - t0;
      printf("test long sleep: requested %.9f sec, actual %.9f sec\n", sleep_requested, sleep_actual);
   }

   return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
