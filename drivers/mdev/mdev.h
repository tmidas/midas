/********************************************************************\

  Name:         mdev.h
  Created by:   Stefan Ritt

  Contents:     Generic base class for MIDAS device drivers
\********************************************************************/

#ifndef MDEV_H
#define MDEV_H

extern int hs_define_panel2(const char *group, const char *panel, const std::vector<std::string> vars,
                            const std::vector<std::string> label, const std::vector<std::string> formula,
                            const std::vector<std::string> color = {});

class mdev {
protected:
   int m_event_id;
   std::string m_equipment_name;

public:
   mdev(std::string equipment_name) {
      m_equipment_name = equipment_name;
      if (!midas::odb::exists("/Equipment/" + equipment_name))
         mthrow("Cannot find equipment \"" + equipment_name + "\" in ODB");

      midas::odb o("/Equipment/" + equipment_name);
      try {
         m_event_id = o["Common"]["Event ID"];
      } catch (mexception &e) {
         mthrow("Cannot find \"Event ID\" under equipment \"" + equipment_name + "\" in ODB");
      }
   }
   virtual ~mdev(void) {} // virtual destructor for proper cleanup

   // getters and setters
   int get_event_id(void) { return m_event_id; }
   std::string get_equipment_name() { return m_equipment_name; }

   // pure virtual functions
   virtual void odb_setup(void) = 0;
   virtual void init(void) = 0;
   virtual void exit(void) = 0;
   virtual void loop(void) = 0;
   virtual int read_event(char *pevent, int off) { return 0; };

   void static mdev_odb_setup(std::vector<mdev *> &mdev_table) {
      // Call odb_setup for all devices
      for (mdev *m : mdev_table)
            m->odb_setup();
   }

   void static mdev_init(std::vector<mdev *> &mdev_table) {
      // Call odb_setup for all devices
      for (mdev *m : mdev_table)
         m->init();
   }

   void static mdev_loop(std::vector<mdev *> &mdev_table) {
      // Call loop() function of all devices to read data
      for (mdev *m : mdev_table)
         m->loop();
   }

   void define_history_panel(std::string panelName,
                             std::vector<std::string> names,
                             std::vector<std::string> labels = {},
                             std::vector<std::string> formula = {},
                             std::vector<std::string> color = {}
   )
   {
      std::vector<std::string> vars;
      for (size_t i=0 ; i<names.size() ; i++)
         vars.push_back(m_equipment_name + std::string(":") + names[i]);

      hs_define_panel2(m_equipment_name.c_str(), panelName.c_str(), vars, labels, formula, color);
   }

};

#endif // MDEV_H