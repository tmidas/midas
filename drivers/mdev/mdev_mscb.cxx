/********************************************************************\

  Name:         mdev_mscb.cxx
  Created by:   Stefan Ritt

  Contents:     MIDAS device drivers class for MSCB devices

\********************************************************************/

#include <iostream>

#include "midas.h"
#include "odbxx.h"

#include "mdev_mscb.h"

void mdev_mscb::odb_setup(void) {
   // MPDC settings in ODB
   midas::odb settings = {
           { "Grid display",  false },
           { "Names MSCB",    std::string(31, '\0')},
           { "Unit MSCB",     std::string(31, '\0')},
           { "Editable",      false },
           { "Group",         false },
           { "MSCB",          {} },
   };

   settings.connect("/Equipment/" + m_equipment_name + "/Settings");
   m_settings.connect("/Equipment/" + m_equipment_name + "/Settings");

   // loop over all nodes
   m_n_variables = 0;

   for (auto & node : m_node) {
      if (!midas::odb::exists("/Equipment/" + m_equipment_name + "/Settings/MSCB/" +
                              node.m_device + "/Enabled"))
         m_settings["MSCB"][node.m_device]["Enabled"] = true;

      node.m_enabled = m_settings["MSCB"][node.m_device]["Enabled"];
      m_settings["MSCB"][node.m_device]["Device"]  = node.m_device;
      m_settings["MSCB"][node.m_device]["Pwd"]     = node.m_pwd;
      m_settings["MSCB"][node.m_device]["Address"] = node.m_address;

      // loop over all variables
      for (int i=0 ; i<(int)node.m_mscb_var.size() ; i++) {

         // check for double names
         for (int j=0 ; j<m_n_variables ; j++)
            if (m_settings["Names MSCB"][j].s() == node.m_mscb_var[i].name)
               mthrow("Variable name \"" + node.m_mscb_var[i].name + "\" defined twice.");

         // set into in ODB
         m_settings["Names MSCB"][m_n_variables].set_string_size(node.m_mscb_var[i].name, 32);
         m_settings["Editable"][m_n_variables] = node.m_mscb_var[i].output;
         m_settings["MSCB"][node.m_device]["Variables"][i] = node.m_mscb_var[i].var;
         m_n_variables++;
      }

      m_settings["MSCB"][node.m_device]["Variables"].resize(node.m_mscb_var.size());
   }

   if (m_n_variables > 0) {
      m_settings["Names MSCB"].resize(m_n_variables);
      m_settings["Editable"].resize(m_n_variables);
      m_settings["Group"] = m_group;
   }

   // MSCB variables in ODB
   m_variables.connect("/Equipment/" + m_equipment_name + "/Variables");
   for (int i=0 ; i<m_n_variables ; i++)
      m_variables["MSCB"][i] = (float) ss_nan();

   // MSCB common tree
   m_common.connect("/Equipment/" + m_equipment_name + "/Common");
}

void mdev_mscb::init(void) {

   if (!m_common["Enabled"]) {
      std::cout << "Equipment \"" << m_equipment_name << "\" disabled" << std::endl;
      return;
   }

   // loop over all MSCB nodes
   int n_variables = 0;
   for (mscb_node &node : m_node) {

      if (!node.m_enabled)
         continue;

      try {

         node.m_mscb = new midas::mscb(node.m_device, node.m_address, node.m_pwd, node.m_enabled);

      } catch (mexception &e) {
         std::string s = "Cannot connect to MSCB device \"" + node.m_device + ":" +
                         std::to_string(node.m_address) + "\"";
         mthrow1(s);
      }

      // loop over all variables for that node
      for (size_t i=0 ; i<node.m_mscb_var.size() ; i++) {

         // retrieve index for variable "var"
         if (node.m_mscb_var[i].var != "") {
            node.m_mscb_var[i].index = node.m_mscb->idx(node.m_mscb_var[i].var);
            if (node.m_mscb_var[i].index == -1) {
               std::string s = "Invalid MSCB variable name \"" + node.m_device + ":" +
                   std::to_string(node.m_address) + ":" + node.m_mscb_var[i].var + "\"";
               mthrow1(s);
            }
         }

         try {

            // retrieve value for mirror
            m_mirror.push_back((float)(*node.m_mscb)[node.m_mscb_var[i].index]);

         } catch (mexception &e) {
            std::string s;
            if (node.m_mscb_var[i].var == "")
               s = "Cannot retrieve MSCB variable \"" + node.m_device + ":" +
                               std::to_string(node.m_address) + ":" + std::to_string(node.m_mscb_var[i].index) + "\"";
            else
               s = "Cannot retrieve MSCB variable \"" + node.m_device + ":" +
                            std::to_string(node.m_address) + ":" + node.m_mscb_var[i].var + "\"";
            mthrow1(s);
         }

         // retrieve unit from MSCB node
         std::string unit = node.m_mscb->get_unit_short(node.m_mscb_var[i].index);

         // overwrite if unit has been passed to constructor
         if (!node.m_mscb_var[i].unit.empty())
            unit = node.m_mscb_var[i].unit;

         // write unit to ODB
         m_settings["Unit MSCB"][n_variables].set_string_size(unit, 32);

         // write format to ODB
         if (node.m_mscb_var[i].format.empty())
            m_settings["Format MSCB"][n_variables].set_string_size("f2", 32);
         else
            m_settings["Format MSCB"][n_variables].set_string_size(node.m_mscb_var[i].format, 32);

         n_variables++;
      }

   }

   // install callback
   if (n_variables > 0)
      m_variables["MSCB"].watch([this](midas::odb &o) {

         int n_var = 0;

         // loop over all MSCB nodes
         for (mscb_node &node : m_node) {
            // loop over all variables for that node
            for (size_t i = 0; i < node.m_mscb_var.size(); i++, n_var++) {

               if (node.m_mscb_var[i].output) {
                  float f = o[n_var];

                  // check if data differs from mirror
                  if (f != m_mirror[n_var] && !ss_isnan(f)) {

                     // call confirm function if present
                     if (node.m_mscb_var[i].confirm != nullptr) {
                        bool confirm = node.m_mscb_var[i].confirm(node.m_mscb_var[i], f);
                        if (!confirm)
                           continue;
                     }

                     // store data in mirror
                     m_mirror[n_var] = f;

                     // write data to MSCB node
                     int index = node.m_mscb_var[i].index;
                     std::cout << "Write \"" << f << "\" to " << node.m_device << ":" << node.m_address << "-" << index << std::endl;

                     (*node.m_mscb)[index] = f;
                  }
               }
            }
         }
      });
}

void mdev_mscb::exit(void) {
}

void mdev_mscb::loop(void) {

   static DWORD last_time_measured = 0;

   // read input values once per second
   if (ss_millitime() - last_time_measured > 1000) {

      if (!m_common["Enabled"])
         return;

      // read all nodes
      int n_var = 0;
      std::string error_message = "";
      for (auto node : m_node) {

         // skip disabled nodes
         if (!node.m_enabled) {
            n_var += node.m_mscb_var.size();
            continue;
         }

         // read all node variables
         auto status = node.m_mscb->read_range();
         if (status != MSCB_SUCCESS) {
            for (size_t i = 0; i < node.m_mscb_var.size(); i++) {
               m_mirror[n_var] = (float) ss_nan();
               m_variables["MSCB"][n_var] = (float) ss_nan();
               n_var++;
            }

            if (!error_message.empty())
               error_message += "\n";
            error_message += "Communication error with \"" +
                            node.m_mscb->get_submaster() + ":" +
                            std::to_string(node.m_mscb->get_node_address()) + "\"";

         } else { // status == MSCB_SUCCESS

            // read all variables from MSCB
            for (size_t i = 0; i < node.m_mscb_var.size(); i++) {
               float f = (*node.m_mscb)[node.m_mscb_var[i].index];
               if (node.m_mscb_var[i].convert != nullptr) {
                  f = node.m_mscb_var[i].convert(f);
               }

               m_mirror[n_var] = f;
               m_variables["MSCB"][n_var] = f;
               n_var++;
            }
         }
      }

      if (!error_message.empty())
         mthrow1(error_message);

      last_time_measured = ss_millitime();
   }

}

int mdev_mscb::read_event(char *pevent, int off) {

   if (!m_common["Enabled"])
      return 0;

   float *pdata;

   // init bank structure
   bk_init32a(pevent);

   // create a bank with values
   bk_create(pevent, "SMSC", TID_FLOAT, (void **)&pdata);
   for (int i = 0; i < m_n_variables; i++)
      *pdata++ = m_variables["MSCB"][i];
   bk_close(pevent, pdata);

   return bk_size(pevent);
}

