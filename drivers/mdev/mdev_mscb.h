/********************************************************************\

  Name:         mdev_mscb.h
  Created by:   Stefan Ritt

  Contents:     MIDAS device drivers class for MSCB devices

\********************************************************************/

#ifndef MDEV_MSCB_H
#define MDEV_MSCB_H

#include "mscbxx.h"
#include "mdev.h"

struct mscb_var {
   std::string var;
   std::string name;
   int         index;
   bool        output;
   std::function<bool(mscb_var, float)> confirm;
   std::function<float(float)> convert;
   std::string unit;
   std::string format;

   mscb_var(std::string &v, std::string &n, int i, bool o, std::string u, std::string fo)
      : var(v), name(n), index(i), output(o), confirm(nullptr), convert(nullptr), unit(u), format(fo) {};
   mscb_var(std::string &n, int i, bool o, std::string u, std::string fo)
      : var(""), name(n), index(i), output(o), confirm(nullptr), convert(nullptr), unit(u), format(fo) {};
   mscb_var(std::string &v, std::string &n, int i, bool o, std::function<bool(mscb_var, float)> f, std::string u, std::string fo)
      : var(v), name(n), index(i), output(o), confirm(f), convert(nullptr), unit(u), format(fo) {};
   mscb_var(std::string &n, int i, bool o, std::function<bool(mscb_var, float)> f, std::string u, std::string fo)
      : var(""), name(n), index(i), output(o), confirm(f), convert(nullptr), unit(u), format(fo) {};
   mscb_var(std::string &v, std::string &n, int i, bool o, std::function<float(float)> f, std::string u, std::string fo)
           : var(v), name(n), index(i), output(o), confirm(nullptr), convert(f), unit(u), format(fo) {};
   mscb_var(std::string &n, int i, bool o, std::function<float(float)> f, std::string u, std::string fo)
           : var(""), name(n), index(i), output(o), confirm(nullptr), convert(f), unit(u), format(fo) {};

};

class mscb_node {
public:
   std::string  m_device;
   std::string  m_pwd;
   int          m_address;
   bool         m_enabled;
   midas::mscb* m_mscb;
   bool         m_group_flag;

   std::vector<mscb_var> m_mscb_var;
   std::vector<bool>     m_group;

public:
   mscb_node(std::string d, int a, std::string pwd = "", bool e=true) :
      m_device(d), m_pwd(pwd), m_address(a), m_enabled(e), m_group_flag(false) {};

   void add_group(void) {
      m_group.push_back(m_group_flag);
      m_group_flag = false;
   }

   void add_input(std::string var, std::string name, std::string unit = "", std::string format = "") {
      m_mscb_var.emplace_back(var, name, 0, false, unit, format);
      add_group();
   }

   void add_input(int index, std::string name, std::string unit = "", std::string format = "") {
      m_mscb_var.emplace_back(name, index, false, unit, format);
      add_group();
   }

   void add_input(std::string var, std::string name, std::function<float(float)> f, std::string unit = "", std::string format = "") {
      m_mscb_var.emplace_back(var, name, 0, false, f, unit, format);
      add_group();
   }

   void add_input(int index, std::string name, std::function<float(float)> f, std::string unit = "", std::string format = "") {
      m_mscb_var.emplace_back(name, index, false, f, unit, format);
      add_group();
   }

   void add_output(std::string var, std::string name, std::string unit = "", std::string format = "") {
      m_mscb_var.emplace_back(var, name, 0, true, unit, format);
      add_group();
   }

   void add_output(int index, std::string name, std::string unit = "", std::string format = "") {
      m_mscb_var.emplace_back(name, index, true, unit, format);
      add_group();
   }

   void add_output(std::string var, std::string name, std::function<bool(mscb_var, float)> f, std::string unit = "", std::string format = "") {
      m_mscb_var.emplace_back(var, name, 0, true, f, unit, format);
      add_group();
   }

   void add_output(int index, std::string name, std::function<bool(mscb_var, float)> f, std::string unit = "", std::string format = "") {
      m_mscb_var.emplace_back(name, index, true, f, unit, format);
      add_group();
   }

   void start_new_group() { m_group_flag = true; }

   std::vector<bool> get_group() { return m_group; };
};

class mdev_mscb : public mdev {

private:
   midas::odb m_settings;
   midas::odb m_variables;
   midas::odb m_common;
   int        m_n_variables;

   std::vector<mscb_node> m_node;
   std::vector<float>     m_mirror;
   std::vector<bool>      m_group;

public:
   mdev_mscb(std::string equipment_name) :
      mdev(equipment_name) {};

   ~mdev_mscb(void) {};

   void add_node(mscb_node n) {
      m_node.push_back(n);

      for (auto g : n.get_group())
         m_group.push_back(g);
   }

   void get_all(int i);

   void odb_setup(void) override;
   void init(void) override;
   void exit(void) override;
   void loop(void) override;
   int read_event(char *pevent, int off) override;
};

#endif // MDEV_MPDC_H