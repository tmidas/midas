/********************************************************************\

  Name:         lv_fe.c
  Created by:   Stefan Ritt

  Contents:     Example slow control fronted using the mdev_mscb driver

\********************************************************************/

#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <math.h>

#include "midas.h"
#include "mfe.h"
#include "odbxx.h"
#include "mscbxx.h"

#include "mdev_mscb.h"

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "MDEV Frontend";
/* The frontend file name, don't change it */
const char *frontend_file_name = __FILE__;

/*-- Equipment list ------------------------------------------------*/

BOOL equipment_common_overwrite = TRUE;

int mdev_loop(void);
int mdev_read(char *pevent, int off);

EQUIPMENT equipment[] = {

   {"MDEV",                     /* equipment name */
    {1, 0,                      /* event ID, trigger mask */
     "SYSTEM",                  /* event buffer */
     EQ_PERIODIC,               /* equipment type */
     0,                         /* event source */
     "MIDAS",                   /* format */
     TRUE,                      /* enabled */
     RO_ALWAYS,
     60000,                     /* read event every 60 sec */
     0,                         /* readout pause */
     0,                         /* number of sub events */
     10,                        /* log history every 10 seconds */
     "", "", ""} ,
     mdev_read                  /* readout routine */
    },

   {""}
};

// master device table
std::vector<mdev *> mdev_table;

/*-- Error dispatcher causing communication alarm -------------------*/

void fe_error(const char *error)
{
   cm_msg(MERROR, "fe_error", "%s", error);
}

/*-- global init function -------------------------------------------*/

// sample confirmation function for output variable
bool voltage_confirm(mscb_var m, float v) {

   if (m.name == "Voltage" && v > 10) { // turn on voltage
      midas::odb e("/Equipment/MDEV/Variables/MSCB");
      float t = e[1];
      if (t > 50) {
         char str[80];
         snprintf(str, sizeof(str), "Cannot turn on voltage, temperature too high (%.1lf\u00B0C)", t);
         fe_error(str);
         return false;
      }
   }

   return true;
}

int frontend_init()
{
   // setup device table --------------------------------------------*/

   auto mscb = new mdev_mscb("MDEV");

   auto n = new mscb_node("mscb394", 43);
   n->add_output("HV", "Voltage", voltage_confirm);
   n->add_input("HVMeas", "Voltage Measured", [](float x) -> float { return x * 2; });
   mscb->add_node(*n);

   mdev_table.push_back(mscb);

   // ----------------------

   // set error dispatcher for alarm functionality
   mfe_set_error(fe_error);

   // install loop handler which will be called continuously
   install_frontend_loop(mdev_loop);

   // setup ODB and initialize all drivers
   try {
      mdev::mdev_odb_setup(mdev_table);
      mdev::mdev_init(mdev_table);
   } catch (mexception & e) {
      cm_msg(MERROR, "frontend_init", "%s", e.what());
      return FE_ERR_HW;
   }

   return FE_SUCCESS;
}

/*-- loop function called continuously ------------------------------*/

int mdev_loop()
{
   // call loop functions of all devices
   mdev::mdev_loop(mdev_table);

   // don't eat all CPU
   ss_sleep(10);

   return FE_SUCCESS;
}

/*-- event readout function -----------------------------------------*/

int mdev_read(char *pevent, int off)
{
   // call read_event routine of device belonging to equipment
   for (mdev *m : mdev_table)
      if (EVENT_ID(pevent) == m->get_event_id())
         return m->read_event(pevent, off);

   return 0;
}
