/*********************************************************************

  Name:         uov1740drv.h
  Created by:   K.Olchanski
                    implementation of the CAENCommLib functions
                P.-A. Amaudruz mode for A4818 USB and CAENDigitizer
  Contents:     v1740 64-channel 50 MHz 12-bit ADC

  $Id$
                
*********************************************************************/
#ifndef  UOV1740DRV_INCLUDE_H
#define  UOV1740DRV_INCLUDE_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <CAENDigitizer.h>
#include "v1740.h"

CAEN_DGTZ_ErrorCode ov1740_GroupSet(int handle, uint32_t channel, uint32_t what, uint32_t that);
CAEN_DGTZ_ErrorCode ov1740_GroupGet(int handle, uint32_t channel, uint32_t what, uint32_t *data);
CAEN_DGTZ_ErrorCode ov1740_GroupGet(int handle, uint32_t channel, uint32_t what, uint32_t *data);
CAEN_DGTZ_ErrorCode ov1740_GroupThresholdSet(int handle, uint32_t channel, uint32_t threshold);
CAEN_DGTZ_ErrorCode ov1740_GroupOUThresholdSet(int handle, uint32_t channel, uint32_t threshold);
CAEN_DGTZ_ErrorCode ov1740_GroupDACSet(int handle, uint32_t channel, uint32_t dac);
CAEN_DGTZ_ErrorCode ov1740_GroupDACGet(int handle, uint32_t channel, uint32_t *dac);
CAEN_DGTZ_ErrorCode ov1740_AcqCtl(int handle, uint32_t operation);
CAEN_DGTZ_ErrorCode ov1740_GroupConfig(int handle, uint32_t operation);
CAEN_DGTZ_ErrorCode ov1740_info(int handle, int *nchannels, uint32_t *data);
CAEN_DGTZ_ErrorCode ov1740_BufferOccupancy(int handle, uint32_t channel, uint32_t *data);
CAEN_DGTZ_ErrorCode ov1740_BufferFree(int handle, int nbuffer, uint32_t *mode);
CAEN_DGTZ_ErrorCode ov1740_Status(int handle);
CAEN_DGTZ_ErrorCode ov1740_Setup(int handle, int mode);


#endif // UOV1740DRV_INCLUDE_H
