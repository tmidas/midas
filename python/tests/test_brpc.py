import unittest
import midas.client
import multiprocessing
import subprocess
import midas.event
import midas.utils.helper_test_brpc
import os
import os.path
import sys
import ctypes

"""
This test is a little painful as we only allow one midas client per process
(and we don't even allow multiple connections using multiprocess). So we create
a client here that registers an RPC handler, and then launch a different script
to call the BRPC function.
"""

def brpc_call_helper(queue):
    helper = midas.utils.helper_test_brpc.__file__
    proc = subprocess.run([sys.executable, helper], capture_output=True)
    
    if proc.returncode != 0:
        print("***** STDOUT OF SUBPROCESS *****")
        print(proc.stdout.decode('utf-8'))
        print("***** STDERR OF SUBPROCESS *****")
        print(proc.stderr.decode('utf-8'))
        print("********************************")

    output_arb = midas.utils.helper_test_brpc.file_path_arb
    output_ev = midas.utils.helper_test_brpc.file_path_ev
    
    if not os.path.exists(output_ev):
        queue.put("FILE DOESN'T EXIST")
    else:
        with open(output_ev, 'rb') as f:
            queue.put(f.read())

    if not os.path.exists(output_arb):
        queue.put("FILE DOESN'T EXIST")
    else:
        with open(output_arb, 'rb') as f:
            queue.put(f.read())

class TestRpc(unittest.TestCase):
    def setUp(self):
        self.client = None
        
    def tearDown(self):
        if self.client:
            self.client.disconnect()
            
        output = midas.utils.helper_test_brpc.file_path_arb
        if os.path.exists(output):
            os.unlink(output)
            
        output = midas.utils.helper_test_brpc.file_path_ev
        if os.path.exists(output):
            os.unlink(output)
          
    def brpc_callback(self, client, cmd, args, max_len):
        print("Handling %s" % args)
        
        if args == "as_event":
            ev = midas.event.Event()
            ev.create_bank("BBBB", midas.TID_INT, [1, 2, 3, 4])
            return (midas.status_codes["SUCCESS"], ev)
        else:
            return (midas.status_codes["SUCCESS"], ctypes.create_string_buffer(b'abc123'))
          
    def testBRPC(self):
        # Register our function
        self.client = midas.client.MidasClient("pytest")
        self.client.register_brpc_callback(self.brpc_callback)
        self.client.communicate(10)
        
        # Call our function from a separate process
        queue = multiprocessing.Queue()
        spawned = multiprocessing.Process(target=brpc_call_helper, args=(queue,))
        spawned.start()

        # Wait for things to complete
        for i in range(30):
            self.client.communicate(100)
            if not queue.empty():
                break
            
        spawned.join()
        
        self.assertFalse(queue.empty())

        ev_raw = queue.get()
        arb = queue.get()

        self.assertNotEqual(ev_raw, "FILE DOESN'T EXIST")
        self.assertNotEqual(arb, "FILE DOESN'T EXIST")

        ev = midas.event.Event()
        ev.unpack(ev_raw)
        self.assertEqual(len(ev.banks), 1)
        self.assertTrue("BBBB" in ev.banks.keys())
        self.assertEqual(len(ev.banks["BBBB"].data), 4)
        self.assertEqual(ev.banks["BBBB"].data[0], 1)
        self.assertEqual(ev.banks["BBBB"].data[1], 2)
        self.assertEqual(ev.banks["BBBB"].data[2], 3)
        self.assertEqual(ev.banks["BBBB"].data[3], 4)

        self.assertEqual(len(arb), 7)
        arb_as_str = arb.decode("utf-8")
        self.assertEqual(arb_as_str[0], 'a')
        self.assertEqual(arb_as_str[5], '3')
          
if __name__ == '__main__':
    unittest.main()